package com.pe.sam.toastme;

import android.content.Context;
import android.widget.Toast;

public class EasyToaster {

    public static void setToast(Context c, String message) {
        Toast.makeText(c,message,Toast.LENGTH_SHORT).show();
    }
}
